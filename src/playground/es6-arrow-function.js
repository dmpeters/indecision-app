// const square = function (x) {
//     return x * x;
// }
// console.log(square(8));

// const squareArrow = (x) => {
//     return x * x;
// }

// const squareArrow = (x) => x * x;
// console.log(squareArrow(9));

// challenge
// use arrow functions
// getFirsName('mike smith') function -> 'mike'
// first verbose syntax
// 2nd expression syntax

// const verboseFirstName = (fullName) => {
//     return fullName.split(' ')[0];
// }
// console.log(verboseFirstName('David Peters'));

// const expressionFirstName = (fullName) => fullName.split(' ')[0];
// console.log(expressionFirstName('Christina Anderson'));

// arguments object - no longer bound with arrow functions
const add = (a, b) => {
    // console.log(arguments);
    return a + b;
}
console.log(add(55, 1, 1001));

// this keyword - no longer bound with arrow functions
const user = {
    'name': 'David',
    'cities': ['Milwaukee', 'Los Angeles', 'Nashville', 'Atlanta'],
    printPlacesLived() {
        return this.cities.map((city) => this.name + ' has lived in ' + city);
    }
}
console.log(user.printPlacesLived());

// Challenge

const multiplier = {
    'numbers': [1, 2, 3],
    'multiplyBy': 2,
    multiply() {
        return this.numbers.map((number) => this.multiplyBy * number);
    }
}
console.log(multiplier.multiply());