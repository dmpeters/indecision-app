console.log('app.js is running');

const app = {
    'title': 'Indecision App',
    'subtitle': 'Let this app radmonly choose',
    'options': []
}

const removeOptions = () => {
    app.options = [];
    renderIndecisionApp();
}

const onMakeDecision = () => {
    const randumNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randumNum];
    alert(option);
    // renderIndecisionApp();
}

const onFormSubmit = (e) => {
    e.preventDefault();
    console.log('form submitted');

    const option = e.target.elements.option.value;

    if (option) {
        app.options.push(option);
        e.target.elements.option.value = '';
        renderIndecisionApp();
    }

};

const appRoot = document.getElementById("app");

const renderIndecisionApp = () => {
    const template = (
        <div>
          <h1>{app.title}</h1>
          {app.subtitle && <p>{app.subtitle}</p>}
          {app.options.length > 0 ? "Here Are Your Options" : 'No Options'}
          
          <button disabled={app.options.length === 0} onClick={onMakeDecision}>What Should I Do?</button>
          <button onClick={removeOptions}>Remove All Options</button>

          <ol>
            {app.options.map((option) => <li key={option}>{option}</li>)}
          </ol>
          
          <form onSubmit={onFormSubmit}>
              <input type="text" name="option" />
              <button>Add Option</button>
          </form>

        </div>
      );

    ReactDOM.render(template, appRoot);
};

renderIndecisionApp();