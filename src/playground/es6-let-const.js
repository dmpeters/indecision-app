var nameVar = 'David';
nameVar = 'Mike';
console.log('nameVar', nameVar);

let nameLet = 'Jed';
console.log('nameLet', nameLet);

const nameConst = 'Bob';
console.log('nameConst', nameConst);

function getPetName(){
    var petName = 'Oliver';
    return petName
}

const petName = getPetName();
console.log(petName);

// Block scoping
const fullName = 'David Peters';

if (fullName) {
    const firstName = fullName.split(' ')[0];
    console.log(firstName);
}

console.log(firstName);