class Visibile extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibile: false
        };
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibile: !prevState.visibile
            };
        });
    }
    
    render() {
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>{this.state.visibile ? "Hide Details" : "Show Details"}</button>
                {this.state.visibile && (
                    <div>
                        <p>Here are some details to show!</p>
                    </div>
                )}
            </div>
        );
    }
}

ReactDOM.render(<Visibile />, document.getElementById("app"));

// let visibile = false;

// const onVisibilityToggle = () => {
//     visibile = !visibile;
//     renderVisibilityApp();
// };

// const renderVisibilityApp = () => {
//     const template = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={onVisibilityToggle}>{visibile ? "Hide Details" : "Show Details"}</button>
//             {visibile && (
//                 <div>
//                     <p>Here are some details to show!</p>
//                 </div>
//             )}
//         </div>
//     );

//     ReactDOM.render(template, document.getElementById("app"));
// };

// renderVisibilityApp();